import ApiError from "../../helpers/ApiError";
import Subject from "../../models/subject/subject.model";
import User from "../../models/user/user.model";
import Department from "../../models/department/department.model";
import Faculty from "../../models/faculty/faculty.model";
import Community from "../../models/community/community.model";
import Lecture from "../../models/lecture/lecture.model";
import News from "../../models/news/news.model";
import Event from "../../models/event/event.model";
import Report from "../../models/reports/report.model";
import Question from "../../models/questions/questions.model";
import Message from "../../models/contact/contact.model";

const populateQuery = [

];
const action = [
    { path: 'user', model: 'user' },
]
export default {
    async getLastUser(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {
                $and: [
                    {deleted: false},
                    {type:'STUDENT'}
                ]
            };
         
            let lastUser = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },
    async getLastActions(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {deleted: false};
         
            let lastUser = await Report.find(query).populate(action)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },

    async getLastLecture(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let { status} = req.query
            let query = {deleted: false };
            if (status)
                query.status = status;                
            let lastLecture = await Lecture.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastLecture);
        } catch (error) {
            next(error);
        }
    },
   
    async count(req,res, next) {
        try {
            let query = { deleted: false };
            const studentsCount = await User.count({deleted: false,type:'STUDENT'});
            const TEACHERSCount = await User.count({deleted: false,type:'TEACHER'});
            const FacultyCount = await Faculty.count(query);
            const communityCount = await Community.count({deleted:false});
            const questionsCount = await Question.count({deleted:false});
            const subjectsCount = await Subject.count({deleted:false});
            const messageCount = await Message.count({deleted:false,reply:false});
            const lecturesCounts = await Lecture.count(query);
            const newsCounts = await News.count(query);
            const eventsCounts = await Event.count(query);

            res.status(200).send({
                studentsCount:studentsCount,
                TEACHERSCount:TEACHERSCount,
                questionsCount:questionsCount,
                subjectsCount:subjectsCount,
                messages:messageCount,
                lecturesCounts:lecturesCounts,

                FacultyCount:FacultyCount,
                communityCount:communityCount,
                
                eventsCounts:eventsCounts,
                newsCounts:newsCounts,
            });
        } catch (err) {
            next(err);
        }
        
    },
    async top(req,res, next) {
        try {
            let query = { deleted: false };
            const topLecture = await Lecture.find(query).sort({buyCount:1});
            const topStudent = await User.find({deleted: false,type:'STUDENT'}).sort({balance:-1});
            const topActiveTeacher = await User.find({deleted: false,type:'TEACHER'}).sort({answersCount:-1});

            res.status(200).send({
                topLecture:topLecture[0],
                topStudent:topStudent[0],
                topActiveTeacher:topActiveTeacher[0],
               
            });
        } catch (err) {
            next(err);
        }
        
    },
    
}
import ApiResponse from "../../helpers/ApiResponse";
import Schedule from "../../models/schedule/schedule.model";
import User from "../../models/user/user.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet} from "../../helpers/CheckMethods";
import { checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";

const populateQuery = [
    { path: 'faculty', model: 'faculty' },
    { path: 'department', model: 'department' },
    { path: 'year', model: 'year' },
    { path: 'lectures.subject', model: 'subject' },
];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { day,date,department,year,faculty} = req.query;
            let query = {deleted: false };
            if(date) {
                let from = date+ 'T00:00:00.000Z';
                let to= date + 'T23:59:00.000Z';
                query = { 
                    dateMillSec: { $gte :Date.parse(from), $lte : Date.parse(to) },
                    deleted:false
               };
            } 
            if(faculty) query.faculty = faculty;
            if(year) query.year = year;
            if(day) query.day = day;
            if(department) query.department = department;
            let Schedules = await Schedule.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const SchedulesCount = await Schedule.count(query);
            const pageCount = Math.ceil(SchedulesCount / limit);

            res.send(new ApiResponse(Schedules, page, pageCount, limit, SchedulesCount, req));
        } catch (err) {
            next(err);
        }
    },
    async getAll(req, res, next) {

        try {
            let { day,date,department,year,faculty} = req.query;
            let query = {deleted: false };
            if(date) {
                let from = date+ 'T00:00:00.000Z';
                let to= date + 'T23:59:00.000Z';
                query = { 
                    dateMillSec: { $gte :Date.parse(from), $lte : Date.parse(to) },
                    deleted:false
               };
            } 
            if(day) query.day = day;
            if(department) query.department = department;
            
            if(faculty) query.faculty = faculty;
            if(year) query.year = year;
            let Schedules = await Schedule.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })

            res.send(Schedules);
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('date').not().isEmpty().withMessage('date is required'),
            body('day').not().isEmpty().withMessage('day is required'),
            body('faculty').not().isEmpty().withMessage('faculty is required'),
            body('year').not().isEmpty().withMessage('year is required'),
            body('department').not().isEmpty().withMessage('department is required'),
            body('lectures').not().isEmpty().withMessage('lectures is required')
            .isLength({ min: 1 }).withMessage('lectures should have at least one element of branches')
            .custom(async (properties, { req }) => {
                for (let prop of properties) {
                    body('subject').not().isEmpty().withMessage('subject is required'),
                    body('time').not().isEmpty().withMessage('time is required')
                }
                return true;
            }),

        ];
        

        return validations;
    }, 

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);
            console.log(validatedBody.lectures)
            validatedBody.dateMillSec = Date.parse(validatedBody.date)
            let createdSchedule = await Schedule.create({ ...validatedBody});

            res.status(201).send(createdSchedule);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { ScheduleId } = req.params;
            await checkExist(ScheduleId, Schedule, { deleted: false });

            const validatedBody = checkValidations(req);
            validatedBody.dateMillSec = Date.parse(validatedBody.date)
            let updatedSchedule = await Schedule.findByIdAndUpdate(ScheduleId, {
                ...validatedBody,
            }, { new: true });
            res.status(200).send(updatedSchedule);
        }
        catch (err) {
            next(err);
        }
    },
    async findById(req, res, next) {
        try {
            let { ScheduleId } = req.params;
            await checkExist(ScheduleId, Schedule, { deleted: false });
            let schedule = await Schedule.findById(ScheduleId);
            res.send(schedule);
        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { ScheduleId } = req.params;
            let schedule = await checkExistThenGet(ScheduleId, Schedule, { deleted: false });
            
            schedule.deleted = true;
            await schedule.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },

};
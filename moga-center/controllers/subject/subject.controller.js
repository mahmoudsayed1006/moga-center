import Subject from "../../models/subject/subject.model";
import User from "../../models/user/user.model";
import Department from "../../models/department/department.model";
import Faculty from "../../models/faculty/faculty.model";
import Question from "../../models/questions/questions.model";
import Year from "../../models/years/years.model";
import Lecture from "../../models/lecture/lecture.model";
import Report from "../../models/reports/report.model";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import ApiResponse from "../../helpers/ApiResponse";
import { body } from "express-validator/check";
import { checkExist ,checkExistThenGet } from "../../helpers/CheckMethods";
const populateQuery = [
    { path: 'faculty', model: 'faculty' },
    { path: 'department', model: 'department' },
    { path: 'year', model: 'year' },

];
export default {
    validateSubjectBody(isUpdate = false) {
        return [
            body('name_en').not().isEmpty().withMessage('english Subject name Required')
                .custom(async (value, { req }) => {
                    let userQuery = { name_en: value, deleted: false };
                    if (isUpdate)
                        userQuery._id = { $ne: req.params.SubjectId };
                    if (await Subject.findOne(userQuery))
                        throw new Error('name_en duplicated');
                    else
                        return true;
                }),
            body('name_ar').not().isEmpty().withMessage('arabic Subject Name Required'),
            body('faculty').not().isEmpty().withMessage('faculty Required'),
            body('department').not().isEmpty().withMessage('department Required'),
            body('year').not().isEmpty().withMessage('year Required'),
            body('division').optional()
               
        ];
    },
    async create(req, res, next) {
        try {
            let user = req.user;
           if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin auth')));
            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.faculty, Faculty, { deleted: false });
            await checkExist(validatedBody.department, Department, { deleted: false });
            await checkExist(validatedBody.year, Year, { deleted: false });
            let subject = await Subject.create({ ...validatedBody })
            let reports = {
                "action":"Create Subject",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(201).send(subject);
        } catch (error) {
            next(error);
        }
    },
    async getAll(req, res, next) {
        try {
            let {year,faculty,department,hasLecture} = req.query;
            let query = {deleted: false };
            if(hasLecture == "true"){
                query = {
                    $and: [
                        {lectureNum: { $gte : 1}},
                        {deleted: false } ,
                    ]
                };
            }
            if (year) query.year = year;
            if (faculty) query.faculty = faculty;
            if (department) query.department = department;
            let subjects = await Subject.find(query).populate(populateQuery);
            subjects.forEach(async(subject) => {
                let theSubject = await checkExistThenGet(subject._id,Subject,{deleted:false})
                let lectueCount = await Lecture.count({subject:subject._id,deleted:false});
                theSubject.lectueNum = lectueCount;
                await theSubject.save()
            });
            subjects = await Subject.find(query).populate(populateQuery);
            return res.status(200).send(subjects);
        } catch (error) {
            next(error);
        }
    },

    async getAllPaginated(req, res, next) {
        try {
           
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {year,faculty,department,hasLecture} = req.query;
            let query = {deleted: false };
            if(hasLecture == "true"){
                query = {
                    $and: [
                        {lectureNum: { $gte : 1}},
                        {deleted: false } ,
                    ]
                };
            }
            if (year) query.year = year;
            if (faculty) query.faculty = faculty;
            if (department) query.department = department;
            let subjects = await Subject.find(query).populate(populateQuery)
                .limit(limit)
                .skip((page - 1) * limit).sort({ _id: -1 });
            subjects.forEach(async(subject) => {
                let theSubject = await checkExistThenGet(subject._id,Subject,{deleted:false})
                let lectueCount = await Lecture.count({subject:subject._id,deleted:false});
                theSubject.lectueNum = lectueCount;
                await theSubject.save()
            });
            subjects = await Subject.find(query).populate(populateQuery)
                .limit(limit)
                .skip((page - 1) * limit).sort({ _id: -1 });
            let count = await Subject.count(query);
            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(subjects, page, pageCount, limit, count, req));
        } catch (error) {
            next(error);
        }
    },

    async update(req, res, next) {
        try {
            let user = req.user;
            let { SubjectId } = req.params;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            const validatedBody = checkValidations(req);
            let Subjects = await Subject.findByIdAndUpdate(SubjectId, { ...validatedBody }).populate(populateQuery);
            let reports = {
                "action":"Update Subject",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(200).send("update success");
        } catch (error) {
            next(error);
        }
    },

    async getById(req, res, next) {
        try {
            let user = req.user;
            let { SubjectId } = req.params;

            if (req.user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let subject = await checkExistThenGet(SubjectId, Subject, { deleted: false });
            return res.send(subject);
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        let { SubjectId} = req.params;

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            let subject = await checkExistThenGet(SubjectId, Subject, { deleted: false });
            let subjects = await Subject.find({ _id: SubjectId });
            for (let subject of subjects) {
                subject.deleted = true;
                await subject.save();
                 //delete questions
                let questions = await Question.find({ subject: subject._id });
                for (let question of questions) {
                    question.deleted = true;
                    await question.save();
                }
                //delete lectures
                let lectures = await Lecture.find({ subject: subject._id });
                for (let lecture of lectures) {
                    lecture.deleted = true;
                    await lecture.save();
                }
            }
            subject.deleted = true;
            await subject.save();
            let reports = {
                "action":"Delete Subject",
            };
            let report = await Report.create({...reports, user: user });
            res.send('delete success');

        } catch (err) {
            next(err);
        }
    },
    async followUp(req, res, next) {
        try {
            let user = req.user;
            let subjects = await Subject.find({
                deleted:false,
                department:user.department,
                faculty:user.faculty,
                year:user.year
            }).distinct('_id')
            console.log(subjects)
            let subjectsFollowUp = []
            for (let SubjectId of subjects) {
                let subject = await checkExistThenGet(SubjectId, Subject, { deleted: false });
                let lectures = await Lecture.find({deleted:false,subject:SubjectId})
                let theLectures = []
                lectures.forEach(async(lecture) => {
                    let data = {}
                    let theLecture = await checkExistThenGet(lecture._id, Lecture, { deleted: false });
                    if(theLecture.type == "HARD-COPY"){
                        data = {
                            "number":theLecture.number,
                            "checked":false
                        }
                    }else{
                        data = {
                            "number":theLecture.number,
                            "checked":true
                        }
                    }
                    theLectures.push(data)
                });
                let lecturesCount = await Lecture.countDocuments({deleted:false,subject:SubjectId})
                console.log(lecturesCount)
                let subjectFollowUp = {
                    "subject":subject,
                    "lecturesCount" : lecturesCount,
                    "theLectures":theLectures
                }
                console.log(subjectFollowUp)
                subjectsFollowUp.push(subjectFollowUp)
            }
           

            return res.send({subjectsFollowUp});
        } catch (error) {
            next(error);
        }
    },
}
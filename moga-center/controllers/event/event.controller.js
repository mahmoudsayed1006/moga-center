import ApiResponse from "../../helpers/ApiResponse";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Event from "../../models/event/event.model";

export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {deleted: false };
            let events = await Event.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const EventsCount = await Event.count(query);
            const pageCount = Math.ceil(EventsCount / limit);

            res.send(new ApiResponse(events, page, pageCount, limit, EventsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findSelection(req, res, next) {
        try {
            let query = { deleted: false };
            let events = await Event.find(query)
                .sort({ createdAt: -1 });
            res.send(events)
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('title_en').not().isEmpty().withMessage('english title is required'),
            body('description_ar').not().isEmpty().withMessage('arabic description is required'),
            body('eventDate').not().isEmpty().withMessage('eventDate is required'),
            body('description_en').not().isEmpty().withMessage('english description is required'),
            body('title_ar').not().isEmpty().withMessage('arabic title is required'),
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
    
            const validatedBody = checkValidations(req);
            console.log(validatedBody);
            validatedBody.eventDateMillSec = Date.parse(validatedBody.eventDate)
            let image;
            if(req.file){
                image = await handleImg(req);
            }
            
            console.log(image);
             console.log(image);
            let createdEvent = await Event.create({ ...validatedBody,img:image});

            let reports = {
                "action":"Create Event",
            };
            let report = await Report.create({...reports, user: user });
            
            res.status(201).send(createdEvent);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { EventId } = req.params;
            await checkExist(EventId, Event, { deleted: false });
            let Event = await Event.findById(EventId);
            res.send(Event);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { EventId } = req.params;
            await checkExist(EventId, Event, { deleted: false });

            const validatedBody = checkValidations(req);
            validatedBody.eventDateMillSec = Date.parse(validatedBody.eventDate)
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            let updatedEvent = await Event.findByIdAndUpdate(EventId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update Ads",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedEvent);
        }
        catch (err) {
            next(err);
        }
    },
    
    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { EventId } = req.params;
            let event = await checkExistThenGet(EventId, Event, { deleted: false });
            
            event.deleted = true;
            await event.save();
            let reports = {
                "action":"Delete Event",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};
import Faculty from "../../models/faculty/faculty.model";
import Department from "../../models/department/department.model";
import Lecture from "../../models/lecture/lecture.model";
import Question from "../../models/questions/questions.model";
import User from "../../models/user/user.model";
import Subject from "../../models/subject/subject.model";
import Report from "../../models/reports/report.model";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import ApiResponse from "../../helpers/ApiResponse";
import { body } from "express-validator/check";
import { checkExist ,checkExistThenGet } from "../../helpers/CheckMethods";

export default {
    validateFacultyBody(isUpdate = false) {
        return [
            body('faculty_en').not().isEmpty().withMessage('english faculty name Required')
                .custom(async (value, { req }) => {
                    let userQuery = { faculty_en: value, deleted: false };
                    if (isUpdate)
                        userQuery._id = { $ne: req.params.FacultyId };
                    if (await Faculty.findOne(userQuery))
                        throw new Error('faculty_en duplicated');
                    else
                        return true;
                }),
            body('faculty_ar').not().isEmpty().withMessage('arabic fculty Name Required'),
            body('yearsNum').not().isEmpty().withMessage('yearsNum is required'),
            body('hasDivision').optional()
            
               
        ];
    },
    async create(req, res, next) {
        try {
            let user = req.user;
           if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin auth')));
            const validatedBody = checkValidations(req);
            let faculty = await Faculty.create({ ...validatedBody });
            let reports = {
                "action":"Create Faculty",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(201).send(faculty);
        } catch (error) {
            next(error);
        }
    },
    async getAll(req, res, next) {
        try {
           
            let faculties = await Faculty.find({ deleted: false });

            return res.status(200).send(faculties);
        } catch (error) {
            next(error);
        }
    },

    async getAllPaginated(req, res, next) {
        try {
            console.log("h")
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let faculties = await Faculty.find({ deleted: false })
                .limit(limit)
                .skip((page - 1) * limit).sort({ _id: -1 });

            let count = await Faculty.count({ deleted: false });

            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(faculties, page, pageCount, limit, count, req));
        } catch (error) {
            next(error);
        }
    },

    async update(req, res, next) {
        try {
            let user = req.user;
            let { FacultyId } = req.params;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            const validatedBody = checkValidations(req);
            let faculty = await Faculty.findByIdAndUpdate(FacultyId, { ...validatedBody });
            let reports = {
                "action":"Update Faculty",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(200).send("update success");
        } catch (error) {
            next(error);
        }
    },

    async getById(req, res, next) {
        try {
            let user = req.user;
            let { FacultyId } = req.params;

            if (req.user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let faculty = await checkExistThenGet(FacultyId, Faculty, { deleted: false });
            return res.send(faculty);
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        let { FacultyId} = req.params;

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            let faculty = await checkExistThenGet(FacultyId, Faculty, { deleted: false });
            let departments = await Department.find({ faculty: FacultyId });
                for (let department of departments) {
                    department.deleted = true;
                    await department.save();
                    //delete subjects
                    let subjects = await Subject.find({ department: department._id });
                    for (let subject of subjects) {
                        subject.deleted = true;
                        await subject.save();
                         //delete questions
                        let questions = await Question.find({ subject: subject._id });
                        for (let question of questions) {
                            question.deleted = true;
                            await question.save();
                        }
                        //delete lectures
                        let lectures = await Lecture.find({ subject: subject._id });
                        for (let lecture of lectures) {
                            lecture.deleted = true;
                            await lecture.save();
                        }
                    }
                   
                    
                }
            faculty.deleted = true;
            await faculty.save();
            let reports = {
                "action":"Delete Faculty",
            };
            let report = await Report.create({...reports, user: user });
            res.send('delete success');

        } catch (err) {
            next(err);
        }
    }
}
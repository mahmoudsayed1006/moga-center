import Community from "../../models/community/community.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import ApiResponse from "../../helpers/ApiResponse";
import { body } from "express-validator/check";
import { checkExist ,checkExistThenGet } from "../../helpers/CheckMethods";
import Subject from "../../models/subject/subject.model";
const populateQuery = [
    {
        path: 'owner', model: 'user',
        populate: { path: 'faculty', model: 'faculty' },
    },
    {
        path: 'owner', model: 'user',
        populate: { path: 'department', model: 'department' },
    },
    {
        path: 'owner', model: 'user',
        populate: { path: 'division', model: 'division' },
    },
    {
        path: 'owner', model: 'user',
        populate: { path: 'year', model: 'year' },
    },

];
export default {
    validateCommunityBody(isUpdate = false) {
        return [
            body('description').not().isEmpty().withMessage('description Required')
            
               
        ];
    },
    async create(req, res, next) {
        try {
            let user = req.user;
           
            const validatedBody = checkValidations(req);
            validatedBody.owner = req.user._id
            let community = await Community.create({ ...validatedBody });
            let reports = {
                "action":"Create Communitys",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(201).send(community);
        } catch (error) {
            next(error);
        }
    },
    async getAll(req, res, next) {
        try {
           
            let communitys = await Community.find({ deleted: false }).populate(populateQuery)
            .sort({ createdAt: -1 });

            return res.status(200).send(communitys);
        } catch (error) {
            next(error);
        }
    },

    async getAllPaginated(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let Communitys = await Community.find({ deleted: false }).populate(populateQuery)
                .limit(limit)
                .skip((page - 1) * limit).sort({ createdAt: -1 });

            let count = await Community.count({ deleted: false });

            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(Communitys, page, pageCount, limit, count, req));
        } catch (error) {
            next(error);
        }
    },

    async update(req, res, next) {
        try {
            let user = req.user;
            let { CommunityId } = req.params;
           
            const validatedBody = checkValidations(req);
            let Communitys = await Community.findByIdAndUpdate(CommunityId, { ...validatedBody });
            let reports = {
                "action":"Update Community",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(200).send("update success");
        } catch (error) {
            next(error);
        }
    },

    async getById(req, res, next) {
        try {
            let user = req.user;
            let { CommunityId } = req.params;

           
            let community = await checkExistThenGet(CommunityId, Community, { deleted: false });
            return res.send(community);
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        let { CommunityId} = req.params;

        try {
            let user = req.user;
            
            let community = await checkExistThenGet(CommunityId, Community, { deleted: false });

            community.deleted = true;
            await community.save();
            let reports = {
                "action":"Delete Community",
            };
            let report = await Report.create({...reports, user: user });
            res.send('delete success');

        } catch (err) {
            next(err);
        }
    }
}
import Question from "../../models/questions/questions.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods";
import { checkValidations,handleImgs } from "../shared/shared.controller";
import { body } from "express-validator/check";
import ApiResponse from "../../helpers/ApiResponse";
import Subject from "../../models/subject/subject.model";
import User from "../../models/user/user.model";
import { toImgUrl } from '../../utils';
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
const populateQuery = [
    { path: 'subject', model: 'subject' },
    {
        path: 'student', model: 'user',
        populate: { path: 'faculty', model: 'faculty' },
    },
    {
        path: 'student', model: 'user',
        populate: { path: 'department', model: 'department' },
    },
    {
        path: 'student', model: 'user',
        populate: { path: 'division', model: 'division' },
    },
    {
        path: 'student', model: 'user',
        populate: { path: 'year', model: 'year' },
    },
    {
        path: 'teacher', model: 'user',
        populate: { path: 'faculty', model: 'faculty' },
    },
    {
        path: 'teacher', model: 'user',
        populate: { path: 'department', model: 'department' },
    },
    {
        path: 'teacher', model: 'user',
        populate: { path: 'year', model: 'year' },
    },
    {
        path: 'teacher', model: 'user',
        populate: { path: 'division', model: 'division' },
    },
    



];
export default {

    async findAll(req, res, next) {

        try {
            try {
                let page = +req.query.page || 1, limit = +req.query.limit || 20,
                {answer,student,teacher,subject}=req.query;
                let query = {deleted: false };
                if(answer == "true") query.answer = true
                if(answer == "false") query.answer = false
               
                if (subject) query.subject = subject;
                if (student) query.student = student;
                if (teacher) query.teacher = teacher;
                let questions = await Question.find(query)//.populate(populateQuery)
                    .sort({ createdAt: -1 })
                    .limit(limit)
                    .skip((page - 1) * limit);
    
    
                const questionsCount = await Question.count(query);
                const pageCount = Math.ceil(questionsCount / limit);
    
                res.send(new ApiResponse(questions, page, pageCount, limit, questionsCount, req));
            } catch (err) {
                next(err);
            }
        } catch (err) {
            next(err);
        }
    },
    async getAll(req, res, next) {

        try {
            try {
                let {answer,student,teacher,subject}=req.query;
                let query = {deleted: false };
                if(answer == "true") query.answer = true
                if(answer == "false") query.answer = false
               
                if (subject) query.subject = subject;
                if (student) query.student = student;
                if (teacher) query.teacher = teacher;
                
                let questions = await Question.find(query).populate(populateQuery)
                    .sort({ createdAt: -1 })
                res.send(questions);
            } catch (err) {
                next(err);
            }
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('question').not().isEmpty().withMessage('question is required'),
            body('subject').not().isEmpty().withMessage('subject is required'),
        ];
        
        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'STUDENT')
                return next(new ApiError(403, ('student.auth')));
    
            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.subject, Subject, { deleted: false });
            validatedBody.student = req.user._id
            let createdquestions = await Question.create({ ...validatedBody});

            let reports = {
                "action":"Create Question ",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdquestions);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let { questionsId } = req.params;
            await checkExist(questionsId, Question, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedquestions = await Question.findByIdAndUpdate(questionsId, {
                ...validatedBody,
            }, { new: true }).populate(populateQuery);
            let reports = {
                "action":"Update Question ",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(200).send(updatedquestions);
        }
        catch (err) {
            next(err);
        }
    },
   
    async delete(req, res, next) {
        try {
            let { questionsId } = req.params;
            let questions = await checkExistThenGet(questionsId, Question, { deleted: false });
            questions.deleted = true;
            await questions.save();
            
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    validateAnswerBody() {
        let validations = [
            body('answer').not().isEmpty().withMessage('answer is required'),
            body('img').optional(),
        ];
        
        return validations;
    },
    async answer(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'TEACHER')
                return next(new ApiError(403, ('teacher.auth')));
            const validatedBody = checkValidations(req);  
            
            let { questionsId } = req.params;
            let questions = await checkExistThenGet(questionsId, Question, { deleted: false });
            if (req.files) {
                if (req.files['img']) {
                    let imagesList = [];
                    for (let imges of req.files['img']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    questions.img = imagesList;
                }
            }
            questions.answer = validatedBody.answer;
            questions.teacher =  req.user._id;
            questions.isAnswer = true;
            await questions.save();
            let teacher = await checkExistThenGet(req.user._id, User, { deleted: false });
            teacher.answersCount = teacher.answersCount + 1;
            
            await teacher.save();
            sendNotifiAndPushNotifi({
                targetUser: questions.student, 
                fromUser: req.user, 
                text: 'new notification',
                subject: questions.id,
                subjectType: 'Your question has been answers'
            });
            let notif = {
                "description_en":'Your question has been answers',
                "description_ar":'تم الاجابه على السؤال الخاص بك',
                "title_ar":"تم الاجابه على سؤالك",
                "title_en":"Your question has been answers"
            }
            await Notif.create({...notif,resource:req.user,target:questions.student,question:questions.id});
            res.status(204).send();

        }
        catch (err) {
            next(err);
        }
    },
};
import Year from "../../models/years/years.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import ApiResponse from "../../helpers/ApiResponse";
import { body } from "express-validator/check";
import { checkExist ,checkExistThenGet } from "../../helpers/CheckMethods";
import Subject from "../../models/subject/subject.model";

export default {
    validateYearBody(isUpdate = false) {
        return [
            body('name_en').not().isEmpty().withMessage('english Year name Required')
                .custom(async (value, { req }) => {
                    let userQuery = { name_en: value, deleted: false };
                    if (isUpdate)
                        userQuery._id = { $ne: req.params.YearId };
                    if (await Year.findOne(userQuery))
                        throw new Error('name_en duplicated');
                    else
                        return true;
                }),
            body('name_ar').not().isEmpty().withMessage('arabic year Name Required'),
            
               
        ];
    },
    async create(req, res, next) {
        try {
            let user = req.user;
           if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin auth')));
            const validatedBody = checkValidations(req);
            let years = await Year.create({ ...validatedBody });
            let reports = {
                "action":"Create Years",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(201).send(years);
        } catch (error) {
            next(error);
        }
    },
    async getAll(req, res, next) {
        try {
           
            let years = await Year.find({ deleted: false });

            return res.status(200).send(years);
        } catch (error) {
            next(error);
        }
    },

    async getAllPaginated(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let years = await Year.find({ deleted: false })
                .limit(limit)
                .skip((page - 1) * limit).sort({ _id: -1 });

            let count = await Year.count({ deleted: false });

            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(years, page, pageCount, limit, count, req));
        } catch (error) {
            next(error);
        }
    },

    async update(req, res, next) {
        try {
            let user = req.user;
            let { YearId } = req.params;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            const validatedBody = checkValidations(req);
            let years = await Year.findByIdAndUpdate(YearId, { ...validatedBody });
            let reports = {
                "action":"Update Years",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(200).send("update success");
        } catch (error) {
            next(error);
        }
    },

    async getById(req, res, next) {
        try {
            let user = req.user;
            let { YearId } = req.params;

            if (req.user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let year = await checkExistThenGet(YearId, Year, { deleted: false });
            return res.send(year);
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        let { YearId} = req.params;

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            let year = await checkExistThenGet(YearId, Year, { deleted: false });
            let subjects = await Subject.find({ year: YearId });
            for (let subject of subjects) {
                subject.deleted = true;
                await subject.save();
            }
            year.deleted = true;
            await year.save();
            let reports = {
                "action":"Delete Years",
            };
            let report = await Report.create({...reports, user: user });
            res.send('delete success');

        } catch (err) {
            next(err);
        }
    }
}
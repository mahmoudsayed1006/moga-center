import Faculty from "../../models/faculty/faculty.model";
import Department from "../../models/department/department.model";
import Lecture from "../../models/lecture/lecture.model";
import Subject from "../../models/subject/subject.model";
import Question from "../../models/questions/questions.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { body } from "express-validator/check";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import { checkExist ,checkExistThenGet} from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import Division from "../../models/division/division.model";
const populateQuery = [
    { path: 'faculty', model: 'faculty' },
    { path: 'division', model: 'division' },
]
export default {
    validateDepartmentBody() {
        return [
            body('department_en').not().isEmpty().withMessage('Department englishName Required'),
            body('department_ar').not().isEmpty().withMessage('arabic Department Name Required'),
            body('faculty').not().isEmpty().withMessage('faculty is Required'),
            body('division').optional(),
        ];
    },
    async create(req, res, next) {
        try {
            let user = req.user;
           
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.faculty, Faculty, { deleted: false });
	        if(validatedBody.division)
            	await checkExist(validatedBody.division, Division, { deleted: false });
            let department = await Department.create({ ...validatedBody });
            let reports = {
                "action":"Create New Department",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(201).send(department);
        } catch (error) {
            next(error);
            send(error);
        }
    },
    async getById(req, res, next) {
        try {
            let user = req.user;
            let { DepartmentId } = req.params;

            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            await checkExist(DepartmentId, Department, { deleted: false });

            let department = await Department.findById(DepartmentId).populate('faculty');
            return res.send(department);
        } catch (error) {
            next(error);
        }
    },
    async update(req, res, next) {
        try {
            let user = req.user;
            let { DepartmentId } = req.params;

            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.faculty, Faculty, { deleted: false });
            if(validatedBody.division)
            	await checkExist(validatedBody.division, Division, { deleted: false });
            let department = await Department.findByIdAndUpdate(DepartmentId, { ...validatedBody });

            let reports = {
                "action":"Update Department",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(200).send(department);
        } catch (error) {
            next(error);
        }
    },

    async getAll(req, res, next) {
        try {
            let { faculty,division } = req.query;
            let query = {deleted:false}
            if(faculty) query.faculty = faculty
            if(division) query.division = division
            let Departments = await Department.find(query).populate(populateQuery);
            return res.send(Departments);
        } catch (error) {
            next(error);
        }
    },

    async getAllPaginated(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { faculty,division } = req.query;
            let query = {deleted:false}
            if(faculty) query.faculty = faculty
            if(division) query.division = division
            let Departments = await Department.find(query).populate(populateQuery)
                .limit(limit)
                .skip((page - 1) * limit).sort({ _id: -1 });
            let count = await Department.count(query);

            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(Departments, page, pageCount, limit, count, req));
        } catch (error) {
            next(error);
        }
    },

    async delete(req, res, next) {
        let { DepartmentId } = req.params;
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            let department = await checkExistThenGet(DepartmentId, Department);
            let subjects = await Subject.find({ department: DepartmentId});
            for (let subject of subjects) {
                subject.deleted = true;
                await subject.save();
                 //delete questions
                let questions = await Question.find({ subject: subject._id });
                for (let question of questions) {
                    question.deleted = true;
                    await question.save();
                }
                //delete lectures
                let lectures = await Lecture.find({ subject: subject._id });
                for (let lecture of lectures) {
                    lecture.deleted = true;
                    await lecture.save();
                }
            }
            department.deleted = true;
            await department.save();
            let reports = {
                "action":"Delete Department",
            };
            let report = await Report.create({...reports, user: user });
            res.send('delete success');

        } catch (err) {
            next(err);
        }
    },


}
import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods';
import { body } from 'express-validator/check';
import { checkValidations, handleImg } from '../shared/shared.controller';
import { generateToken } from '../../utils/token';
import ApiResponse from "../../helpers/ApiResponse";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import bcrypt from 'bcryptjs';
import config from '../../config';
import { sendForgetPassword } from '../../services/message-service';
import { generateVerifyCode } from '../../services/generator-code-service';
import Notif from "../../models/notif/notif.model";
import { sendEmail } from "../../services/emailMessage.service";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import { sendMsg } from '../../services/sms-misr';
import moment from "moment";
const checkUserExistByPhone = async (phone) => {
    let user = await User.findOne({ phone });
    if (!user)
        throw new ApiError.BadRequest('Phone Not Found');

    return user;
}
const checkUserExistByEmail = async (email) => {
    let user = await User.findOne({ email });
    if (!user)
        throw new ApiError.BadRequest('email Not Found');

    return user;
}
const populateQuery = [
    { path: 'faculty', model: 'faculty' },
    { path: 'department', model: 'department' },
    { path: 'year', model: 'year' },
    {path: 'division', model: 'division' },
    {path: 'subject', model: 'subject' },

];
export default {
    async addToken(req,res,next){
        try{
            let user = req.user;
            let users = await checkExistThenGet(user.id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.token;
            });
            if(!found){
                users.token.push(req.body.token);
                await users.save();
            console.log(req.body.token);
            }
            res.status(200).send({
                users,
            });
            
        } catch(err){
            next(err);
        }
    },
    async signIn(req, res, next) {
        try{
            let user = req.user;
            user = await User.findById(user.id).populate(populateQuery);
            if(!user)
                return next(new ApiError(403, ('phone or password incorrect')));
            if(req.body.token != null && req.body.token !=""){
                let arr = user.token; 
                var found = arr.find(function(element) {
                    return element == req.body.token;
                });
                if(!found){
                    user.token.push(req.body.token);
                    await user.save();
                }
            }
            if(user.udId != "null" && req.body.udId != null && req.body.udId !=""){
                if(user.udId != req.body.udId){
                    return next(new ApiError(403, ('you can able to login from this device')));
                }
            }
            if(user.udId == "null"){
                user.udId = req.body.udId
                await user.save();
            }
            res.status(200).send({
                user: await User.findById(user.id).populate(populateQuery),
                token: generateToken(user.id)
            });
            
            let reports = {
                "action":"User Login",
            };

            let report = await Report.create({...reports, user: user });
        } catch(err){
            next(err);
        }
    },

    validateUserCreateBody(isUpdate = false) {
        let validations = [
            body('firstname').not().isEmpty().withMessage('firstname is required'),
            body('lastname').not().isEmpty().withMessage('lastname is required'),
            body('faculty').optional(),
            body('division').optional(),
            body('department').optional(),
            body('year').optional(),
            body('token').optional(),
            body('subject').optional(),
            body('udId').optional(),
            body('email').optional()
                .isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),
            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),

            body('type').not().isEmpty().withMessage('type is required')
                .isIn(['STUDENT','TEACHER','ADMIN']).withMessage('wrong type'),
        ];
        if (!isUpdate) {
            validations.push([
                body('password').optional()
            ]);
        }
        return validations;
    },
    async signUp(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            if(validatedBody.type == "STUDENT" && validatedBody.year == null)
                return next(new ApiError(500, ('year is required')));
            if(validatedBody.type == "STUDENT" && validatedBody.faculty == null)
                return next(new ApiError(500, ('division is required')));
            if(validatedBody.type == "STUDENT" && validatedBody.department == null)
                return next(new ApiError(500, ('department is required')));
            if(validatedBody.type == "TEACHER" && validatedBody.subject == null)
                return next(new ApiError(500, ('subject is required')));
            if (req.file) {
               let image = await handleImg(req)
               validatedBody.img = image;
            }
            validatedBody.updatePhoneDate = new Date();
            let createdUser = await User.create({
                ...validatedBody
            });
            let user = await checkExistThenGet(createdUser.id, User,{deleted: false });
            let code =  generateVerifyCode(); 
            if(code.toString().length < 4){
                code = generateVerifyCode(); 
            }else{
                user.verifycode = code
            }
            console.log(code)
            
            await user.save();
            //send code
            console.log(user.verifycode)
            let message =  '  رمز التفعيل الخاص بك هو :  ' + user.verifycode
            sendMsg(message,user.phone)
            res.status(201).send({
                user: await User.findById(createdUser.id).populate(populateQuery),
                token: generateToken(createdUser.id)
            });
            let reports = {
                "action":"User Sign Up",
            };
            let report = await Report.create({...reports, user: createdUser.id });

        } catch (err) {
            next(err);
        }
    },
    async block(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.block = true;
            await activeUser.save();
            let reports = {
                "action":"block User",
            };
            let  report = await Report.create({...reports, user: user });
            res.send('user block');
        } catch (error) {
            next(error);
        }
    },
    async unblock(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.block = false;
            await activeUser.save();
            let reports = {
                "action":"Active User",
            };
            let report = await Report.create({...reports, user: user });
            res.send('user active');
        } catch (error) {
            next(error);
        }
    },
    async active(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.active = true;
            await activeUser.save();
            let reports = {
                "action":"Active User",
            };
            let report = await Report.create({...reports, user: user });
            sendNotifiAndPushNotifi({
                targetUser: userId, 
                fromUser: req.user, 
                text: 'new notification',
                subject: activeUser.id,
                subjectType: 'MoGa Center active your account'
            });
            let notif = {
                "description_en":'MoGa Center active your account',
                "description_ar":'تم تفعيل الحساب الخاص بك',
                "title_en":'تفعيل الحساب',
                "title_ar":"account activation"
            }
            await Notif.create({...notif,resource:req.user,target:userId,user:activeUser.id});
            res.send('user active');
            
        } catch (error) {
            next(error);
        }
    },

    async disactive(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.active = false;
            await activeUser.save();
            let reports = {
                "action":"Dis-Active User",
            };
            let report = await Report.create({...reports, user: user });
            res.send('user disactive');
        } catch (error) {
            next(error);
        }
    },

    async findById(req, res, next) {
        try {
            let { id } = req.params;
            await checkExist(id, User, { deleted: false });

            let user = await User.findById(id).populate(populateQuery);
            res.send({user});
        } catch (error) {
            next(error);
        }
    },

    async checkExistEmail(req, res, next) {
        try {
            let email = req.body.email;
            if (!email) {
                return next(new ApiError(400, 'email is required'));
            }
            let exist = await User.findOne({ email: email });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            let reports = {
                "action":"User Check Email Exist Or Not",
            };
            let report = await Report.create({...reports, user: req.user });
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },

    async checkExistPhone(req, res, next) {
        try {
            let phone = req.body.phone;
            if (!phone) {
                return next(new ApiError(400, 'phone is required'));
            }
            let exist = await User.findOne({ phone: phone });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            let reports = {
                "action":"User Check Mobile Exist Or Not",
            };
            let report = await Report.create({...reports, user: req.user });
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },


    validateUpdatedPassword(isUpdate = false) {
        let validation = [
            body('newPassword').optional().not().isEmpty().withMessage('newPassword is required'),
            body('currentPassword').optional().not().isEmpty().withMessage('currentPassword is required')
           
        ];

        return validation;
    },
    async updatePassword(req, res, next) {
        try {
            let user = await checkExistThenGet(req.user._id, User);
            if (req.body.newPassword) {
                if (req.body.currentPassword) {
                    if (bcrypt.compareSync(req.body.currentPassword, user.password)) {
                        user.password = req.body.newPassword;
                    }
                    else {
                        res.status(400).send({
                            error: [
                                {
                                    location: 'body',
                                    param: 'currentPassword',
                                    msg: 'currentPassword is invalid'
                                }
                            ]
                        });
                    }
                }
            }
            await user.save();
            res.status(200).send({
                user: await User.findById(req.user._id)
            });

        } catch (error) {
            next(error);
        }
    },
    validateSendCode() {
        return [
            body('email').not().isEmpty().withMessage('email Required')
        ];
    },
    async sendCodeToEmail(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);
            user.verifycode = generateVerifyCode(); 
            await user.save();
            //send code
            let text = user.verifycode.toString();
            let description = 'MoGa Center verfication code';
            sendEmail(validatedBody.email, text,description)
            res.status(204).send();
        } catch (error) {
            next(error);
        }
    },
    validateConfirmVerifyCode() {
        return [
            body('verifycode').not().isEmpty().withMessage('verifycode Required'),
            body('email').not().isEmpty().withMessage('email Required'),
        ];
    },
    async resetPasswordConfirmVerifyCode(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);
            if (user.verifycode != validatedBody.verifycode)
                return next(new ApiError.BadRequest('verifyCode not match'));
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },

    validateResetPassword() {
        return [
            body('email').not().isEmpty().withMessage('email is required'),
            body('newPassword').not().isEmpty().withMessage('newPassword is required')
        ];
    },

    async resetPassword(req, res, next) {
        try {

            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);

            user.password = validatedBody.newPassword;
            user.verifyCode = '0000';
            await user.save();
           
            res.status(204).send();

        } catch (err) {
            next(err);
        }
    },

    async updateToken(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.newToken;
            });
            if(!found){
                users.token.push(req.body.newToken);
                await users.save();
            }
            let oldtoken = req.body.oldToken;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == oldtoken){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    async logout(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            let token = req.body.token;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == token){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {type, active,year,faculty,department,name,sort,search} = req.query;
            let query = {deleted: false };
            if(name) {
                query = {
                    $and: [
                        { $or: [
                            {firstname: { $regex: '.*' + name + '.*' , '$options' : 'i' }}, 
                            {lastname: { $regex: '.*' + name + '.*' , '$options' : 'i' }}, 
                          
                          ] 
                        },
                        {deleted: false},
                    ]
                };
            }
            if(search) {
                query = {
                    $and: [
                        { $or: [
                            {firstname: { $regex: '.*' + search + '.*' , '$options' : 'i' }}, 
                            {lastname: { $regex: '.*' + search + '.*' , '$options' : 'i' }}, 
                            {phone: { $regex: '.*' + search + '.*' , '$options' : 'i' }}, 
                          
                          ] 
                        },
                        {deleted: false},
                    ]
                };
            }
            if (type) query.type = type;
            if (active) query.active = active;
            if (year) query.year = year;
            if (faculty) query.faculty = faculty;
            if (department) query.department = department;
            let sortt = { createdAt: -1 }
            if(sort == "answers"){
                sortt = {answersCount:-1}
            }
            let users = await User.find(query).populate(populateQuery)
                .sort(sortt)
                .limit(limit)
                .skip((page - 1) * limit);
            const usersCount = await User.count(query);
            const pageCount = Math.ceil(usersCount / limit);
            res.send(new ApiResponse(users, page, pageCount, limit, usersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllWithoutPagenation(req, res, next) {
        try {
            let {type, active,year,faculty,department,name,sort,search} = req.query;
            let query = {deleted: false };
            if(name) {
                query = {
                    $and: [
                        { $or: [
                            {firstname: { $regex: '.*' + name + '.*' , '$options' : 'i' }}, 
                            {lastname: { $regex: '.*' + name + '.*' , '$options' : 'i' }}, 
                          
                          ] 
                        },
                        {deleted: false},
                    ]
                };
            }
            if(search) {
                query = {
                    $and: [
                        { $or: [
                            {firstname: { $regex: '.*' + search + '.*' , '$options' : 'i' }}, 
                            {lastname: { $regex: '.*' + search + '.*' , '$options' : 'i' }}, 
                            {phone: { $regex: '.*' + search + '.*' , '$options' : 'i' }}, 
                          
                          ] 
                        },
                        {deleted: false},
                    ]
                };
            }
            if (type) query.type = type;
            if (active) query.active = active;
            if (year) query.year = year;
            if (faculty) query.faculty = faculty;
            if (department) query.department = department;
            let sortt = { createdAt: -1 }
            if(sort == "answers"){
                sortt = {answersCount:-1}
            }
            let users = await User.find(query).populate(populateQuery)
                
            res.send(users);
        } catch (err) {
            next(err);
        }
    },
    validateNotif() {
        let validations = [
            body('description')
        ];
        return validations;
    },
    async SendNotif(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let users = await User.find({'type':'CLIENT'});
            users.forEach(user => {
                sendNotifiAndPushNotifi({
                    targetUser: user.id, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: validatedBody.description,
                    subjectType: 'send notification to all users'
                });
                let notif = {
                    "description":validatedBody.description,
                    "arabicDescription":validatedBody.description
                }
                Notif.create({...notif,resource:req.user._id,target:user.id});
            });
            let reports = {
                "action":"send notification to all users",
            };
            let report = await Report.create({...reports, user: req.user._id });
            res.status(200).send('notification send');
        } catch (error) {
            next(error)
        }
    },
    async delete(req, res, next) {
        try {
            let {userId } = req.params;
            if (req.user.type != 'ADMIN')
            return next(new ApiError(403, ('admin.auth'))); 
            let user = await checkExistThenGet(userId, User,
                {deleted: false });
            user.deleted = true
            await user.save();
            let reports = {
                "action":"Delete user",
            };
            let report = await Report.create({...reports, user: req.user._id });
            res.status(204).send();
        }
        catch (err) {
            next(err);
        }
    },
    async addBalance(req, res, next) {
        try {
            let {userId } = req.params;
            if (req.user.type != 'ADMIN')
            return next(new ApiError(403, ('admin.auth'))); 
            let user = await checkExistThenGet(userId, User,
                {deleted: false });
            user.balance = user.balance + parseInt(req.body.cost)
            await user.save();
            let reports = {
                "action":"Add Money to user's balance",
            };
            let report = await Report.create({...reports, user: req.user._id });
            res.status(204).send();
        }
        catch (err) {
            next(err);
        }
    },
    async removeUdId(req, res, next) {
        try {
            let {userId } = req.params;
            if (req.user.type != 'ADMIN')
            return next(new ApiError(403, ('admin.auth'))); 
            let user = await checkExistThenGet(userId, User,
                {deleted: false });
            user.udId = "null"
            await user.save();
            let reports = {
                "action":"Remove  user UdId",
            };
            let report = await Report.create({...reports, user: req.user._id });
            res.status(204).send();
        }
        catch (err) {
            next(err);
        }
    },
    validateUpdatedUser(isUpdate = true) {
        let validation = [
            body('firstname').optional(),
            body('lastname').optional(),
            body('faculty').optional(),
            body('subject').optional(),
            body('department').optional(),           
            body('year').optional(),
            body('password').optional(),
            body('division').optional(),
            body('email').optional()
                .custom(async (value, { req }) => {
                    let {userId} = req.params;
                    let user = await checkExistThenGet(userId, User);
                    let userQuery = { email: value };
                    if (isUpdate && user.email === value)
                        userQuery._id = { $ne: userId };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
            }),
            body('phone').optional()
                .custom(async (value, { req }) => {
                    let {userId} = req.params;
                    let user = await checkExistThenGet(userId, User);
                    let userQuery = { phone: value };
                    if (isUpdate && user.phone === value)
                        userQuery._id = { $ne: userId };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
            }),
            body('type').not().isEmpty().withMessage('type is required')
                .isIn(['STUDENT','TEACHER','ADMIN']).withMessage('wrong type'),

        ];
        if (isUpdate)
            validation.push([
                body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
            ]);

        return validation;
    },
    async updateUserInfo(req, res, next) {
        try {
           
            const validatedBody = checkValidations(req);
            let {userId} = req.params;
            let user = await checkExistThenGet(userId, User);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            if(validatedBody.phone){
                if(req.user.type != 'ADMIN'){
                    user.phone = validatedBody.phone;
                }else{
                    if(user.enableUpdatePhone == true){
                        user.phone = validatedBody.phone;
                    }else{
                        var now = moment();//now
                        var updatePhoneDate = moment(user.updatePhoneDate);
                        let days = updatePhoneDate.diff(now, 'days');
                        if(days > 0){
                            user.updatePhoneDate = new Date();
                            user.enableUpdatePhone = false
                            return next(new ApiError(500, ("you can't update your phone "))); 
                        }else{
                            user.phone = validatedBody.phone;
                            user.updatePhoneDate = new Date();
                            user.enableUpdatePhone = true
                        }
                        
                    }
                }
            }
            
            
            if(validatedBody.subject){
                user.subject = validatedBody.subject;
            }
            if(validatedBody.year){
                user.year = validatedBody.year;
            }
            if(validatedBody.faculty){
                user.faculty = validatedBody.faculty; 
            }
            if(validatedBody.department){
                user.department = validatedBody.department;
            }
            if(validatedBody.division){
                user.division = validatedBody.division;
            }
            if(validatedBody.password){
                user.password = validatedBody.password;
            }
           
            if(validatedBody.firstname){
                user.firstname = validatedBody.firstname;
            }
            if(validatedBody.lastname){
                user.lastname = validatedBody.lastname;
            }
            if(validatedBody.email){
                user.email = validatedBody.email;
            }
           
            if(validatedBody.img){
                user.img = validatedBody.img;
            }
           
            
            await user.save();
            let reports = {
                "action":"Update User Info",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(200).send({
                user: await User.findOne(user).populate(populateQuery),
            });


        } catch (error) {
            next(error);
        }
    },
   
};

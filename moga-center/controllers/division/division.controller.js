import Division from "../../models/division/division.model";
import Faculty from "../../models/faculty/faculty.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { body } from "express-validator/check";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import { checkExist ,checkExistThenGet} from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import Subject from "../../models/subject/subject.model";
import Question from "../../models/questions/questions.model";
import Lecture from "../../models/lecture/lecture.model";
import Department from "../../models/department/department.model";
import Year from "../../models/years/years.model";
const populateQuery = [
    { path: 'faculty', model: 'faculty' },
    { path: 'year', model: 'year' },
]
export default {
    validateBody() {
        return [
            body('division_en').not().isEmpty().withMessage('Division english Name Required'),
            body('division_ar').not().isEmpty().withMessage('arabic Division Name Required'),
            body('faculty').not().isEmpty().withMessage('faculty is Required'),
            body('year').not().isEmpty().withMessage('year is Required'),
            
        ];
    },
    async create(req, res, next) {
        try {
            let user = req.user;
            
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.faculty, Faculty);
            await checkExist(validatedBody.year, Year);
            let division = await Division.create({ ...validatedBody });
            let reports = {
                "action":"Create New Division",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(201).send(division);
        } catch (error) {
            next(error);
            send(error);
        }
    },
    async getById(req, res, next) {
        try {
            let user = req.user;
            let { DivisionId } = req.params;

            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            await checkExist(DivisionId, Division, { deleted: false });

            let Division = await Division.findById(DivisionId).populate('faculty');
            return res.send(Division);
        } catch (error) {
            next(error);
        }
    },
    async update(req, res, next) {
        try {
            let user = req.user;
            let { DivisionId } = req.params;

            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.faculty, Faculty);
            await checkExist(validatedBody.year, Year);
            let division = await Division.findByIdAndUpdate(DivisionId, { ...validatedBody });

            let reports = {
                "action":"Update Division",
            };
            let report = await Report.create({...reports, user: user });
            return res.status(200).send(division);
        } catch (error) {
            next(error);
        }
    },

    async getAll(req, res, next) {
        try {
            let { faculty,year } = req.query;
            let query = {deleted:false}
            if(faculty) query.faculty = faculty
            if(year) query.year = year
            let Divisions = await Division.find(query).populate(populateQuery);
            return res.send(Divisions);
        } catch (error) {
            next(error);
        }
    },

    async getAllPaginated(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { faculty,year } = req.query;
            let query = {deleted:false}
            if(faculty) query.faculty = faculty
            if(year) query.year = year

            let Divisions = await Division.find(query)
            .populate(populateQuery)
                .limit(limit)
                .skip((page - 1) * limit).sort({ _id: -1 });
            let count = await Division.count(query);

            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(Divisions, page, pageCount, limit, count, req));
        } catch (error) {
            next(error);
        }
    },

    async delete(req, res, next) {
        let { DivisionId } = req.params;
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
               return next(new ApiError(403, ('admin.auth')));
            let division = await checkExistThenGet(DivisionId, Division);
                let departments = await Department.find({ division: DivisionId });
                for (let department of departments) {
                    department.deleted = true;
                    await department.save();
                    //delete subjects
                    let subjects = await Subject.find({ department: department._id });
                    for (let subject of subjects) {
                        subject.deleted = true;
                        await subject.save();
                         //delete questions
                        let questions = await Question.find({ subject: subject._id });
                        for (let question of questions) {
                            question.deleted = true;
                            await question.save();
                        }
                        //delete lectures
                        let lectures = await Lecture.find({ subject: subject._id });
                        for (let lecture of lectures) {
                            lecture.deleted = true;
                            await lecture.save();
                        }
                    }
                   
                    
                }

            
           
            division.deleted = true;
            await division.save();
            let reports = {
                "action":"Delete Division",
            };
            let report = await Report.create({...reports, user: user });
            res.send('delete success');

        } catch (err) {
            next(err);
        }
    },


}
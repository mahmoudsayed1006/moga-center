import Lecture from "../../models/lecture/lecture.model";
import Subject from "../../models/subject/subject.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods";
import { checkValidations,handleImgs } from "../shared/shared.controller";
import { body } from "express-validator/check";
import ApiResponse from "../../helpers/ApiResponse";
import { toImgUrl } from "../../utils";
import User from "../../models/user/user.model";

const populateQuery = [
    { path: 'faculty', model: 'faculty' },
    { path: 'division', model: 'division' },
    { path: 'department', model: 'department' },
    { path: 'year', model: 'year' },
    {path: 'subject', model: 'subject'},
  
];
export default {

    async findAll(req, res, next) {

        try {
            try {
                let page = +req.query.page || 1, limit = +req.query.limit || 20,
                {type,subject,year,department,faculty,sort,title,owner}=req.query;
                let query = {deleted: false };
                if(title) {
                    query = {
                        $and: [
                            {title: { $regex: '.*' + title + '.*' , '$options' : 'i' }}, 
                            {deleted: false},
                        ]
                    };
                }
                if (owner) query.owner = owner;
                if (type) query.type = type;
                if (faculty) query.faculty = faculty;
                if (department) query.department = department;
                if (year) query.year = year;
                if (subject) query.subject = subject;
                let sortt = { createdAt: -1 }
                if(sort == "buyCount"){
                    sortt = {answersCount:-1}
                }
                let Lectures = await Lecture.find(query).populate(populateQuery)
                    .sort(sortt)
                    .limit(limit)
                    .skip((page - 1) * limit);
    
    
                const LecturesCount = await Lecture.count(query);
                const pageCount = Math.ceil(LecturesCount / limit);
               
                res.send(new ApiResponse(Lectures, page, pageCount, limit, LecturesCount, req));
            } catch (err) {
                next(err);
            }
        } catch (err) {
            next(err);
        }
    },
    async findAllWithToken(req, res, next) {

        try {
            try {
                let page = +req.query.page || 1, limit = +req.query.limit || 20,
                {type,subject,year,department,faculty,sort,title,owner}=req.query;
                let query = {deleted: false };
                if(title) {
                    query = {
                        $and: [
                            {title: { $regex: '.*' + title + '.*' , '$options' : 'i' }}, 
                            {deleted: false},
                        ]
                    };
                }
                if (owner) query.owner = owner;
                if (type) query.type = type;
                if (faculty) query.faculty = faculty;
                if (department) query.department = department;
                if (year) query.year = year;
                if (subject) query.subject = subject;
                let sortt = { createdAt: -1 }
                if(sort == "buyCount"){
                    sortt = {answersCount:-1}
                }
                let Lectures = await Lecture.find(query).populate(populateQuery)
                    .sort(sortt)
                    .limit(limit)
                    .skip((page - 1) * limit);
                for (let lecture of Lectures) {
                    let theLecture = await checkExistThenGet(lecture._id, Lecture);
                    if(req.user){
                        let myUser = await checkExistThenGet(req.user._id, User);
                        let arr = myUser.buyLectures;
                        var found = arr.find(function(element) {
                            return element == lecture._id;
                        });
                        if(found){
                            theLecture.isBuy = true
                        } else{
                            theLecture.isBuy = false
                        }
                    }
                    await theLecture.save();
                };
                Lectures = await Lecture.find(query).populate(populateQuery)
                .sort(sortt)
                .limit(limit)
                .skip((page - 1) * limit);
                const LecturesCount = await Lecture.count(query);
                const pageCount = Math.ceil(LecturesCount / limit);
               
                res.send(new ApiResponse(Lectures, page, pageCount, limit, LecturesCount, req));
            } catch (err) {
                next(err);
            }
        } catch (err) {
            next(err);
        }
    },
    async getAll(req, res, next) {

        try {
            try {
                let  {type,subject,year,department,faculty,title,count,owner}=req.query;
                let query = {deleted: false };
                if(title) {
                    query = {
                        $and: [
                            {title: { $regex: '.*' + title + '.*' , '$options' : 'i' }}, 
                            {deleted: false},
                        ]
                    };
                }
                if (owner) query.owner = owner;
                if (type) query.type = type;
                if (faculty) query.faculty = faculty;
                if (department) query.department = department;
                if (year) query.year = year;
                if (subject) query.subject = subject;
                let sortt = { createdAt: -1 }
                if(sort == "buyCount"){
                    sortt = {answersCount:-1}
                }
                let lectures = await Lecture.find(query).populate(populateQuery)
                    .sort(sortt)
                res.send(lectures);
            } catch (err) {
                next(err);
            }
        } catch (err) {
            next(err);
        }
    },
    async findById(req, res, next) {
        try {
            let { LectureId } = req.params;
            await checkExist(LectureId, Lecture, { deleted: false });
            let lecture = await Lecture.findById(LectureId);
            res.send(lecture);
        } catch (err) {
            next(err);
        }
    },
    validateBody(isUpdate = false) {
        let validations = [
            body('title').not().isEmpty().withMessage('title is required'),
            body('faculty').not().isEmpty().withMessage('faculty is required'),
            body('year').not().isEmpty().withMessage('year is required'),
            body('department').not().isEmpty().withMessage('department is required'),
            body('division').not().isEmpty().withMessage('division is required'),
            body('subject').not().isEmpty().withMessage('subject is required'),
            body('number').not().isEmpty().withMessage('number is required'),
            body('cost').not().isEmpty().withMessage('cost is required'),

        ];
        if (isUpdate)
        validations.push([
            body('files').optional(),
            body('video').optional()
        ]);

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type == 'STUDENT')
                return next(new ApiError(403, ('admin or teacher auth')));
    
            const validatedBody = checkValidations(req);
            validatedBody.owner = req.user._id
            if (req.files) {
                if (req.files['files']) {
                    let imagesList = []; 
                    for (let imges of req.files['files']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.files = imagesList;
                    validatedBody.type = 'SOFT-COPY'
                }
                if (req.files['video']) {
                    let imagesList = [];
                    for (let imges of req.files['video']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.video = imagesList;
                    validatedBody.type = 'SOFT-COPY'
                }
            }
            let subject = await checkExistThenGet(validatedBody.subject,Subject,{deleted:false})
            subject.lectureNum = subject.lectureNum + 1;
            await subject.save();
            let createdLectures = await Lecture.create({ ...validatedBody});

            let reports = {
                "action":"Create Lecture ",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdLectures);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let user = req.user;
            let { LectureId } = req.params;
            await checkExist(LectureId, Lecture, { deleted: false });
            if (user.type == 'STUDENT')
                return next(new ApiError(403, ('admin or teacher auth')));
    
            const validatedBody = checkValidations(req);
            validatedBody.owner = req.user._id
            if (req.files) {
                if (req.files['files']) {
                    let imagesList = [];
                    for (let imges of req.files['files']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.files = imagesList;
                    validatedBody.type = 'SOFT-COPY'
                }
                if (req.files['video']) { 
                    let imagesList = [];
                    for (let imges of req.files['video']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.video = imagesList; 
                    validatedBody.type = 'SOFT-COPY'
                }
            }
            let updatedLectures = await Lecture.findByIdAndUpdate(LectureId, {
                ...validatedBody,
            }, { new: true }).populate(populateQuery);
            let reports = {
                "action":"Update Lecture ",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(200).send(updatedLectures);
        }
        catch (err) {
            next(err);
        }
    },
   
    async delete(req, res, next) {
        try {
            let { LectureId } = req.params;
            let lecture = await checkExistThenGet(LectureId, Lecture, { deleted: false });
            lecture.deleted = true;
            
            await lecture.save();
            
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    async buy(req, res, next) {
        try {
            let { LectureId } = req.params;
            let user = await checkExistThenGet(req.user._id, User, { deleted: false });
            let lecture = await checkExistThenGet(LectureId, Lecture, { deleted: false });
            let arr = user.buyLectures
            let found2 = arr.find(e => e == LectureId)
            console.log(user.buyLectures)
            console.log(found2)
            if(!found2){
                if(user.balance < lecture.cost)
                    return next(new ApiError(500, ('user balance not enough')));
                lecture.buyCount = lecture.buyCount + 1;
                await lecture.save();
                let arr = user.buyLectures
                arr.push(LectureId)
                user.buyLectures =  arr;
                console.log(arr)
                user.balance = user.balance - lecture.cost
                await user.save();
            }
            res.status(200).send({lecture});

        }
        catch (err) {
            next(err);
        }
    },
    async multiBuy(req, res, next) {
        try {
            let { LectureId } = req.params;
            let users = await User.find({phone:req.body.phones})
            users.forEach(async(val) => {
                let user = await checkExistThenGet(val._id, User, { deleted: false });
                let lecture = await checkExistThenGet(LectureId, Lecture, { deleted: false });
                /*check if lecture exist on user lectures*/
                let arr = user.buyLectures
                let found2 = arr.find(e => e == LectureId)
                if(!found2){
                    /*add user in lecture viwers*/
                    lecture.buyCount = lecture.buyCount + 1;
                    let arr2 = lecture.viewers 
                    arr2.push(val._id)
                    lecture.viewers = arr2 
                    await lecture.save();
                    /*add lecture in user buy lectures array */
                    let arr = user.buyLectures
                    arr.push(LectureId)
                    user.buyLectures =  arr;
                    await user.save();
                }
            });
            res.status(200).send();

        }
        catch (err) {
            next(err);
        }
    },
    
};
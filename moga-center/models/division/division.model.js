import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';


const divisionSchema = new Schema({

    _id: {
        type: Number,
        required: true
    },
    division_en: {
        type: String,
        required: true,
        trim: true
    },
    division_ar: {
        type: String,
        required: true,
        trim: true
    },
    faculty: {
        type: Number,
        ref: 'faculty'
    },
    year: {
        type: [Number],
        ref: 'year'
    },
    deleted:{
        type:Boolean,
        default:false
    }
});

divisionSchema.set('toJSON', {
    transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.deleted;
    }
});


divisionSchema.plugin(autoIncrement.plugin, { model: 'division', startAt: 1 });

export default mongoose.model('division', divisionSchema);

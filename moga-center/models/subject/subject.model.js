import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';

const subjectSchema = new Schema({

    _id: {
        type: Number,
        required: true
    },
    name_en: {
        type: String,
        required: true,
        trim: true,
    },
    name_ar: {
        type: String,
        required: true,
        trim: true,
    },
    lectureNum: {
        type: Number,
        default: 0,
    },
    faculty:{
        type:Number,
        ref:'faculty',
        required: true,
    },
    division: {
        type: [Number],
        ref: 'division'
    },
    department:{
        type:[Number],
        ref:'department',
        required: true,
    },

    year:{
        type:Number,
        ref:'year',
        required: true,
    },
    deleted:{
        type:Boolean,
        default:false
    }
});

subjectSchema.set('toJSON', {
    transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.deleted;
    }
});



subjectSchema.plugin(autoIncrement.plugin, { model: 'subject', startAt: 1 });

export default mongoose.model('subject', subjectSchema);

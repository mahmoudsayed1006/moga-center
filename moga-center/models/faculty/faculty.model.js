import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';

const facultySchema = new Schema({

    _id: {
        type: Number,
        required: true
    },
    faculty_en: {
        type: String,
        required: true,
        trim: true,
    },
    faculty_ar: {
        type: String,
        required: true,
        trim: true,
    },
    yearsNum: {
        type: Number,
        required: true,
    },
    hasDivision:{
        type:Boolean,
        default:false
    },
    deleted:{
        type:Boolean,
        default:false
    }
});

facultySchema.set('toJSON', {
    transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.deleted;
    }
});



facultySchema.plugin(autoIncrement.plugin, { model: 'faculty', startAt: 1 });

export default mongoose.model('faculty', facultySchema);

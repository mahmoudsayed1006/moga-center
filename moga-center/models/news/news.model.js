import mongoose, { Schema } from "mongoose";
import { isImgUrl } from "../../helpers/CheckMethods";
import autoIncrement from 'mongoose-auto-increment';
const NewsSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    img: [{
        type: String,
        //required: true,
        
    }],
    
    title_en:{
        type:String,
        required:true
    },
    title_ar:{
        type:String,
        required:true
    },
    description_ar:{
        type:String,
        required:true
    },
    
    description_en:{
        type:String,
        required:true
    },
   
    visible: {
        type: Boolean,
        default: false
    },
   
    deleted:{
        type:Boolean,
        default:false
    },

},{ timestamps: true });
NewsSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
NewsSchema.plugin(autoIncrement.plugin, { model: 'news', startAt: 1 });

export default mongoose.model('news', NewsSchema);
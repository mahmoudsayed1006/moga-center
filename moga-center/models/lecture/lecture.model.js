import mongoose, { Schema } from "mongoose";
import { isImgUrl } from "../../helpers/CheckMethods";
import autoIncrement from 'mongoose-auto-increment';
const LectureSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    files: [{
        type: String,
    }],
    video: [{
        type: String,
    }],
    title:{
        type:String,
        required:true
    },
    type:{
        type:String,
        required:true,
        default:'HARD-COPY'
    },
    faculty:{
        type:Number,
        ref:'faculty',
        required:true,
        default:1
    },

    owner:{
        type:Number,
        ref:'user',
        default:1
    },
    subject:{
        type:Number,
        ref:'subject',
        required:true
    },
    division: {
        type: Number,
        ref: 'division'
    },
    department:{
        type:Number,
        ref:'department',
        required:true,
        default:1
    },
    year:{
        type:Number,
        ref:'year',
        required:true,
        default:3
    },
    number: {
        type: Number,
        required: true,
    },
    buyCount: {
        type: Number,
        default: 0,
    },
    viewers:{
        type:[Number],
        ref:'user',
    },
    cost: {
        type: Number,
        default: 0,
    },
    isBuy:{
        type:Boolean,
        default:false
    },
    deleted:{
        type:Boolean,
        default:false
    },

},{ timestamps: true });
LectureSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
LectureSchema.plugin(autoIncrement.plugin, { model: 'lecture', startAt: 1 });

export default mongoose.model('lecture', LectureSchema);
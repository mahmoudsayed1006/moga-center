import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const scheduleSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    day:{
        type:String,
        required: true
    },
    department: { 
        type: Number,
        required: true,
        ref:'department'
    },
    faculty:{
        type:Number,
        ref:'faculty',
        required:true,
        default:1
    },
    year:{
        type:Number,
        ref:'year',
        required:true,
        default:3
    },
    date:{
        type: String,
    },
    dateMillSec:{
        type: Number,
    },
    lectures: [
        new Schema({
            subject: { 
                type: Number,
                required: true,
                ref:'subject'
            },
            time: {
                type: String,
                required: true,
            },
        }, { _id: false })
    ],
    
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

scheduleSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;

    }
});
autoIncrement.initialize(mongoose.connection);
scheduleSchema.plugin(autoIncrement.plugin, { model: 'schedule', startAt: 1 });

export default mongoose.model('schedule', scheduleSchema);
import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const QuestionSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    question: {
        type: String,
        required: true,
    },
    subject:{
        type:Number,
        ref:'subject',
        required: true,
    },
    student:{
        type:Number,
        ref:'user',
        required: true,
    },
    teacher:{
        type:Number,
        ref:'user',
    },
    answer:{
        type: String,
    },
    img: {
        type: [String],
    },
    isAnswer:{
        type: Boolean,
        default: false,
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

QuestionSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
QuestionSchema.plugin(autoIncrement.plugin, { model: 'question', startAt: 1 });

export default mongoose.model('question', QuestionSchema);
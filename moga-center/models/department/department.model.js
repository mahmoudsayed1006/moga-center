import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';


const departmentSchema = new Schema({

    _id: {
        type: Number,
        required: true
    },
    department_en: {
        type: String,
        required: true,
        trim: true
    },
    department_ar: {
        type: String,
        required: true,
        trim: true
    },
    faculty: {
        type: Number,
        ref: 'faculty'
    },
    division: {
        type: [Number],
        ref: 'division'
    },
    deleted:{
        type:Boolean,
        default:false
    }
});

departmentSchema.set('toJSON', {
    transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.deleted;
    }
});


departmentSchema.plugin(autoIncrement.plugin, { model: 'department', startAt: 1 });

export default mongoose.model('department', departmentSchema);

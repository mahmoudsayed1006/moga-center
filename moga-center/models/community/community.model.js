import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';

const communitySchema = new Schema({

    _id: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true,
        trim: true,
    },
    owner: {
        type: Number,
        required: true,
        ref:'user'
    },

    deleted:{
        type:Boolean,
        default:false
    }
});

communitySchema.set('toJSON', {
    transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.deleted;
    }
});



communitySchema.plugin(autoIncrement.plugin, { model: 'community', startAt: 1 });

export default mongoose.model('community', communitySchema);

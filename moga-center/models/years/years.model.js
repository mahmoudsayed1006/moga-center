import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';

const yearSchema = new Schema({

    _id: {
        type: Number,
        required: true
    },
    name_en: {
        type: String,
        required: true,
        trim: true,
    },
    name_ar: {
        type: String,
        required: true,
        trim: true,
    },

    deleted:{
        type:Boolean,
        default:false
    }
});

yearSchema.set('toJSON', {
    transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.deleted;
    }
});



yearSchema.plugin(autoIncrement.plugin, { model: 'year', startAt: 1 });

export default mongoose.model('year', yearSchema);

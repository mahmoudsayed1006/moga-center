import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';
import bcrypt from 'bcryptjs';
import isEmail from 'validator/lib/isEmail';
import { isImgUrl } from "../../helpers/CheckMethods";

const userSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    firstname: {
        type: String,
        required: true,
        trim: true
    },
    lastname: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        trim: true,
        required: true,
        validate: {
            validator: (email) => isEmail(email),
            message: 'Invalid Email Syntax'
        }
    },
    phone: {
        type: String,
        trim: true,
        required: true,
    },
    password: {
        type: String,
        required: true
    },
    type: {
        type: String,
        enum: ['STUDENT','TEACHER', 'ADMIN'],
        required:true
    },
    udId:{
        type:String,
        default:"null",
    },
    balance:{
        type:Number,
        default:0,
    },
    answersCount:{
        type:Number,
        default:0,
    },
    faculty:{
        type:[Number],
        ref:'faculty',
    },
    division:{
        type:[Number],
        ref:'division',
    },
    department:{
        type:[Number],
        ref:'department',
    },
    year:{
        type:Number,
        ref:'year',
    },
    subject:{
        type:[Number],
        ref:'subject',
    },
    buyLectures:{
        type:[Number],
        ref:'lecture',
    },
    block:{
        type: Boolean,
        default: false
    },
    active:{
        type: Boolean,
        default: true
    },
    img: {
        type: String,
        default:""
    },
    verifycode: {
        type: Number
    },
    token:{
        type:[String],
    },
    enableUpdatePhone:{
        type: Boolean,
        default: true
    },
    updatePhoneDate: {
        type: Date
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true, discriminatorKey: 'kind' });

userSchema.pre('save', function (next) {
    const account = this;
    if (!account.isModified('password')) return next();

    const salt = bcrypt.genSaltSync();
    bcrypt.hash(account.password, salt).then(hash => {
        account.password = hash;
        next();
    }).catch(err => console.log(err));
});

userSchema.methods.isValidPassword = function (newPassword, callback) {
    let user = this;
    bcrypt.compare(newPassword, user.password, function (err, isMatch) {
        if (err)
            return callback(err);
        callback(null, isMatch);
    });
};

userSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret.password;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
userSchema.plugin(autoIncrement.plugin, { model: 'user', startAt: 1 });
export default mongoose.model('user', userSchema);
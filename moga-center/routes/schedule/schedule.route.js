import express from 'express';
import ScheduleController from '../../controllers/schedule/schedule.controller';
import { requireAuth } from '../../services/passport';
import { parseStringToArrayOfObjectsMw } from '../../utils';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        //parseStringToArrayOfObjectsMw('lectures'),
        ScheduleController.validateBody(),
        ScheduleController.create
    ).get(ScheduleController.findAll);  
router.route('/get/WithoutPagenation')
    .get(ScheduleController.getAll);  
router.route('/:ScheduleId')
    .get(ScheduleController.findById)
    .delete( requireAuth,ScheduleController.delete)
    .put(
        requireAuth,
        //parseStringToArrayOfObjectsMw('lectures'),
        ScheduleController.validateBody(),
        ScheduleController.update
    );
export default router;
import express from 'express';
import FacultyController from '../../controllers/faculty/faculty.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();


router.route('/')
    .post(
        requireAuth,
        FacultyController.validateFacultyBody(),
        FacultyController.create
    )
    .get(FacultyController.getAll);

router.route('/:FacultyId')
    .put(
        requireAuth,
        FacultyController.validateFacultyBody(true),
        FacultyController.update
    )
    .get(requireAuth,FacultyController.getById)
    .delete(requireAuth,FacultyController.delete);
    
router.route('/get/withPaginated')
    .get(FacultyController.getAllPaginated);


export default router;
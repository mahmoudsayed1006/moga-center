import express from 'express';
import { requireSignIn, requireAuth } from '../../services/passport';
import UserController from '../../controllers/user/user.controller';
import { multerSaveTo } from '../../services/multer-service';
import { parseStringToArrayOfObjectsMw } from '../../utils';
const router = express.Router();


router.post('/signin', requireSignIn, UserController.signIn);

router.route('/signup')
    .post(
        multerSaveTo('users').single('img'),
        parseStringToArrayOfObjectsMw('faculty'),
        parseStringToArrayOfObjectsMw('subject'),
        parseStringToArrayOfObjectsMw('division'),
        parseStringToArrayOfObjectsMw('department'),
        UserController.validateUserCreateBody(),
        UserController.signUp
    );
router.route('/find')
    .get(UserController.findAll);

router.route('/findWithoutPagenation/get')
    .get(UserController.findAllWithoutPagenation);
    
router.route('/:id/getUser')
    .get(UserController.findById);

router.route('/:userId/delete')
    .delete(requireAuth,UserController.delete);

router.route('/:userId/block')
    .put(
        requireAuth,
        UserController.block
    );
router.route('/:userId/unblock')
    .put(
        requireAuth,
        UserController.unblock
    );
    router.route('/:userId/active')
    .put(
        requireAuth,
        UserController.active
    );
router.route('/:userId/dis-active')
    .put(
        requireAuth,
        UserController.disactive
    );
router.route('/logout')
    .post(
        requireAuth,
        UserController.logout
    );
router.route('/sendnotif')
    .post(
        requireAuth,
        UserController.validateNotif(),
        UserController.SendNotif
    );
router.route('/addToken')
    .post(
        requireAuth,
        UserController.addToken
    );
router.route('/updateToken')
    .put(
        requireAuth,
        UserController.updateToken
    );

router.route('/:userId/addBalance')
    .put(
        requireAuth,
        UserController.addBalance
    );
router.route('/:userId/removeUdId')
    .put(
        requireAuth,
        UserController.removeUdId
    );


router.put('/check-exist-email', UserController.checkExistEmail);

router.put('/check-exist-phone', UserController.checkExistPhone);


router.put('/user/:userId/updateInfo',
    requireAuth,
    multerSaveTo('users').single('img'),
    parseStringToArrayOfObjectsMw('faculty'),
    parseStringToArrayOfObjectsMw('subject'),
    parseStringToArrayOfObjectsMw('division'),
    parseStringToArrayOfObjectsMw('department'),
    UserController.validateUpdatedUser(true),
    UserController.updateUserInfo);


router.put('/user/updatePassword',
    requireAuth,
    UserController.validateUpdatedPassword(),
    UserController.updatePassword);

router.post('/sendCode',
    UserController.validateSendCode(),
    UserController.sendCodeToEmail);

router.post('/confirm-code',
    UserController.validateConfirmVerifyCode(),
    UserController.resetPasswordConfirmVerifyCode);

router.post('/reset-password',
    UserController.validateResetPassword(),
    UserController.resetPassword);

export default router;

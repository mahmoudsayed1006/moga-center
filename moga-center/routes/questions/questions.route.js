import express from 'express';
import QuestionsController from '../../controllers/questions/questions.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        QuestionsController.validateBody(),
        QuestionsController.create
    )
    .get(QuestionsController.findAll);
    
router.route('/:questionsId')
    .put(
        requireAuth,
        QuestionsController.validateBody(true),
        QuestionsController.update
    )
    .delete( requireAuth,QuestionsController.delete);

router.route('/:questionsId/answer')
    .put(
        requireAuth,
        multerSaveTo('answers').fields([
            { name: 'img', maxCount: 10, options: false },
        ]),
        QuestionsController.validateAnswerBody(),
        QuestionsController.answer
    )
router.route('/withoutPagenation/get')
    .get(QuestionsController.getAll);

export default router;
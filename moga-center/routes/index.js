import express from 'express';
import userRoute from './user/user.route';
import ContactRoute  from './contact/contact.route';
import ReportRoute  from './reports/report.route';
import NotifRoute  from './notif/notif.route';
import AboutRoute  from './about/about.route';
import AdminRoute  from './admin/admin.route';
import FacultyRoute  from './faculty/faculty.route';
import YearRoute  from './years/years.route';
import SubjectRoute  from './subject/subject.route';
import QuestionRoute  from './questions/questions.route';
import CommunityRoute  from './community/community.route';
import NewsRoute  from './news/news.route';
import LectureRoute  from './lecture/lecture.route';
import EventRoute  from './event/event.route';
import ScheduleRoute  from './schedule/schedule.route';
import DivisionRoute  from './division/division.route';
import DepartmentRoute  from './department/department.route';

import { requireAuth } from '../services/passport';

const router = express.Router();

router.use('/', userRoute);
router.use('/community',CommunityRoute);
router.use('/contact-us',ContactRoute);
router.use('/reports',requireAuth, ReportRoute);
router.use('/notif',requireAuth, NotifRoute);
router.use('/admin',requireAuth, AdminRoute);
router.use('/about',AboutRoute);
router.use('/years',YearRoute);
router.use('/faculties',FacultyRoute);
router.use('/divisions',DivisionRoute);
router.use('/departments',DepartmentRoute);
router.use('/subjects',SubjectRoute);
router.use('/lectures',LectureRoute);
router.use('/questions',QuestionRoute);
router.use('/news',NewsRoute);
router.use('/events',EventRoute);
router.use('/schedules',ScheduleRoute);

export default router;

import express from 'express';
import YearController from '../../controllers/years/years.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();


router.route('/')
    .post(
        requireAuth,
        YearController.validateYearBody(),
        YearController.create
    )
    .get(YearController.getAll);

router.route('/:YearId')
    .put(
        requireAuth,
        YearController.validateYearBody(true),
        YearController.update
    )
    .get(requireAuth,YearController.getById)
    .delete(requireAuth,YearController.delete);
    
router.route('/get/withPaginated')
    .get(YearController.getAllPaginated);


export default router;
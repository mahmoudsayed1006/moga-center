import express from 'express';
import EventController from '../../controllers/event/event.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('Event').single('img'),
        EventController.validateBody(),
        EventController.create
    )
    .get(EventController.findAll);
    
router.route('/:EventId')
    .put(
        requireAuth,
        multerSaveTo('Event').single('img'),
        EventController.validateBody(true),
        EventController.update
    )
    .get(EventController.findById)
    .delete( requireAuth,EventController.delete);




export default router;
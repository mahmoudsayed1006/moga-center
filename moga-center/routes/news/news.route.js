import express from 'express';
import NewsController from '../../controllers/news/news.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('News').single('img'),
        NewsController.validateBody(),
        NewsController.create
    )
    .get(NewsController.findAll);
    
router.route('/:newsId')
    .put(
        requireAuth,
        multerSaveTo('News').single('img'),
        NewsController.validateBody(true),
        NewsController.update
    )
    .get(NewsController.findById)
    .delete( requireAuth,NewsController.delete);




export default router;
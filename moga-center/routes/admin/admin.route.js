import express from 'express';
import adminController from '../../controllers/admin/admin.controller';

const router = express.Router();
router.route('/users')
    .get(adminController.getLastUser);

router.route('/lectures')
    .get(adminController.getLastLecture);

router.route('/actions')
    .get(adminController.getLastActions);

router.route('/count')
    .get(adminController.count);

router.route('/top')
    .get(adminController.top);



export default router;

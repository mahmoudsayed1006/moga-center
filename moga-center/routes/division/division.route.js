import express from 'express';
import DivisionController from '../../controllers/division/division.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        DivisionController.validateBody(),
        DivisionController.create
    )
    .get(DivisionController.getAll);
    
router.route('/:DivisionId')
    .put(
        requireAuth,
        DivisionController.validateBody(true),
        DivisionController.update
    )
    .delete( requireAuth,DivisionController.delete);

router.route('/get/withPaginated')
    .get(DivisionController.getAllPaginated);

export default router;
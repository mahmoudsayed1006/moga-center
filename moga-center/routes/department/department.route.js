import express from 'express';
import DepartmentController from '../../controllers/department/department.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        DepartmentController.validateDepartmentBody(),
        DepartmentController.create
    )
    .get(DepartmentController.getAll);

router.route('/get/withPaginated')
    .get(DepartmentController.getAllPaginated);
router.route('/:DepartmentId')
    .put(
        requireAuth,
        DepartmentController.validateDepartmentBody(true),
        DepartmentController.update
    )
    .get(DepartmentController.getById)
    .delete(requireAuth,DepartmentController.delete);




export default router;
import express from 'express';
import SubjectController from '../../controllers/subject/subject.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();


router.route('/')
    .post(
        requireAuth,
        SubjectController.validateSubjectBody(),
        SubjectController.create
    )
    .get(SubjectController.getAll);

router.route('/:SubjectId')
    .put(
        requireAuth,
        SubjectController.validateSubjectBody(true),
        SubjectController.update
    )
    .get(requireAuth,SubjectController.getById)
    .delete(requireAuth,SubjectController.delete);
    
router.route('/get/withPaginated')
    .get(SubjectController.getAllPaginated);

router.route('/get/followUp')
    .get(requireAuth,SubjectController.followUp);
export default router;
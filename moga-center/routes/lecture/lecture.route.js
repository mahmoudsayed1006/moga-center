import express from 'express';
import LectureController from '../../controllers/lecture/lecture.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('lecture').fields([
            { name: 'files', maxCount: 10, options: false },
            { name: 'video', maxCount: 10, options: false }
        ]),
        LectureController.validateBody(),
        LectureController.create
    )
    .get(LectureController.findAll);

router.route('/findAllWithToken')
    .get(requireAuth,LectureController.findAllWithToken);
    
router.route('/:LectureId')
    .put(
        requireAuth,
        multerSaveTo('lecture').fields([
            { name: 'files', maxCount: 10, options: false },
            { name: 'video', maxCount: 10, options: false }
        ]),
        LectureController.validateBody(true),
        LectureController.update
    )
    .get(LectureController.findById)
    .delete( requireAuth,LectureController.delete);

router.route('/:LectureId/buy')
    .put(
        requireAuth,
        LectureController.buy
    )

router.route('/:LectureId/multiBuy')
    .put(
        requireAuth,
        LectureController.multiBuy
    )

export default router;
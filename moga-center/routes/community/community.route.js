import express from 'express';
import CommunityController from '../../controllers/community/community.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();


router.route('/')
    .post(
        requireAuth,
        CommunityController.validateCommunityBody(),
        CommunityController.create
    )
    .get(CommunityController.getAll);

router.route('/:CommunityId')
    .put(
        requireAuth,
        CommunityController.validateCommunityBody(true),
        CommunityController.update
    )
    .get(requireAuth,CommunityController.getById)
    .delete(requireAuth,CommunityController.delete);
    
router.route('/get/getAllPaginated')
    .get(CommunityController.getAllPaginated);


export default router;